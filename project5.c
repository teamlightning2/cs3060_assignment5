/*
 File Header
 Name: Jason Patterson & Daniel Bates
 Class: CS 3060-001
 Program: Assignment 5
 
 The more1 program displays the contents of a file to the terminal one screenful at a time.
 */

#include<stdio.h>
#include<unistd.h>
#include<signal.h>

/*
Parent Signal Handler
parameters: int signum
purpose: prints certain message depending on signum
returns: none 
*/
void parent_SigHandler(int signum);

/*
 Child Signal Handler
 parameters: int signum
 purpose: prints certain message depending on signum
 returns: none 
 */
void child_SigHandler(int signum);

/*
 Child Main Function
 parameters: none
 purpose: the main process for the child
 returns: none 
 */
void child_main();

/*
 Parent Main Function
 parameters: Child process ID
 purpose: the main process for the Parent
 returns: none 
 */
void parent_main(int childID);


int main(int argc, char *argv[])
{
    printf("Jason Patterson & Daniel Bates\n");
    printf("CS 3060-001\n");
    printf("Assignment 5\n");

    /*parent signal handlers*/
    signal(SIGUSR1, parent_SigHandler);
    signal(SIGUSR2, parent_SigHandler);

    int childID;
    
    if((childID = fork()) == -1)
    {
        perror("Fork failed.");
        return 1;
    }else if(childID == 0)
    {
        //This is executed if the process is the child
        child_main();
        //printf("Child\n");
    }else{
        // called for parent process
        parent_main(childID);
    }
    
    return 0;
}

void child_main()
{
    //child signal handler setup
    struct sigaction signalStruct;
    signalStruct.sa_handler = child_SigHandler;
    sigaction(SIGUSR1, &signalStruct, NULL);
    sigaction(SIGUSR2, &signalStruct, NULL);
    sigset_t UserSigSet;
    sigfillset(&UserSigSet);
    sigdelset(&UserSigSet, SIGUSR1);
    
    //Print first message then suspend
    printf("Child process is now active and is waiting for a 'task start' signal from the parent.\n");
    sigsuspend(&UserSigSet);
    
    //Recieved SIGUSR1 signal
    printf("Now sending parent 'task started' signal.\n");
    sleep(3);
    
    //Send SIGUSR1 signal to parent
    kill(getppid(), SIGUSR1);
    sleep(1);
    
    /*
     This is where the child does its task
     */
    
    //Print that task is completed then suspend
    printf("Child task now completed.\n");
    sigfillset(&UserSigSet);
    sigdelset(&UserSigSet, SIGUSR2);
    sigsuspend(&UserSigSet);
    
    //Recieved SIGUSR2 signal
    printf("Now sending parent 'task completed' signal.\n");
    
    //Send SIGUSR2 signal to parent
    kill(getppid(), SIGUSR2);
    
    //Sleep then return
    sleep(2);
    printf("Child process terminating.\n");
}

void parent_main(int childID)
{
    //parent signal handler setup
    struct sigaction signalStruct;
    signalStruct.sa_handler = parent_SigHandler;
    sigaction(SIGUSR1, &signalStruct, NULL);
    sigaction(SIGUSR2, &signalStruct, NULL);
    sigset_t UserSigSet;
    sigfillset(&UserSigSet);
    sigdelset(&UserSigSet, SIGUSR1);
    
    //prints child is created and sleeps
    printf("Child process created.\n");
    sleep(3);

    //tells child to start a task and sends SIGUSR1 signal and waits
    printf("Telling Child to start 'a task'.\n");
    kill(childID, SIGUSR1);
    sigsuspend(&UserSigSet);

    //sleeps and sends SIGUSR2 and message and waits for SIGUSR2
    sleep(3);
    kill(childID, SIGUSR2);
    printf("SIGUSR2 has been sent to child. Waiting for to receive SIGUSR2.\n");

    sigfillset(&UserSigSet);
    sigdelset(&UserSigSet, SIGUSR2);
    sigsuspend(&UserSigSet);
    printf("Received SIGUSR2.\n");
    sleep(3);
    printf("Terminating.\n");

}

void parent_SigHandler(int signum)
{
    if (signum == SIGUSR1)
    {
        printf("***Parent SIGUSR1 handler - Received a task started signal from child***\n");
    }
    else if (signum == SIGUSR2)
    {
	printf("***Parent SIGUSR2 handler - Received a task completed signal from the child***\n");
    }
}

void child_SigHandler(int signum)
{
    if (signum == SIGUSR1)
    {
        printf("**** Child SIGUSR1 handler - Received a 'task start' signal from the parent process ****\n");
    }
    else if (signum == SIGUSR2)
    {
        printf("**** Child SIGUSR2 handler - Received a 'task complete verification' signal from the parent ****\n");
    }
}
